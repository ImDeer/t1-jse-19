package t1.dkhrunina.tm;

import t1.dkhrunina.tm.component.Bootstrap;

public final class Application {

    public static void main(final String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}