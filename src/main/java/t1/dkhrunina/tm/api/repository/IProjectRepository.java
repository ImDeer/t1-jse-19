package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}