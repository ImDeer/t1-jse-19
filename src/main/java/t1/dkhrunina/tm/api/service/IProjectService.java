package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}