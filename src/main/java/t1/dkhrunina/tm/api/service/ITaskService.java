package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}