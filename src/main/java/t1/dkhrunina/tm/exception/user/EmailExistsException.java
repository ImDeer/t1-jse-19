package t1.dkhrunina.tm.exception.user;

public class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error: user with this email already exists");
    }

}