package t1.dkhrunina.tm.command.project;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-remove-by-id";

    private static final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[Remove project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        getProjectTaskService().removeProjectById(id);
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}