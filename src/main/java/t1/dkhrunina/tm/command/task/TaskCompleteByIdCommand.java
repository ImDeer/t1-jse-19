package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "t-complete-by-id";

    private static final String DESCRIPTION = "Complete task by id.";

    @Override
    public void execute() {
        System.out.println("[Complete task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}