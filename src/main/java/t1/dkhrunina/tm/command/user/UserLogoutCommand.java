package t1.dkhrunina.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "u-logout";

    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[User logout]");
        getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}