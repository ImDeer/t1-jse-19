package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "u-change-pwd";

    private static final String DESCRIPTION = "Change current user password.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[Change user password]");
        System.out.println("Enter new password: ");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}