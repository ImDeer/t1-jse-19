package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private static final String NAME = "u-login";

    private static final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[User login]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}