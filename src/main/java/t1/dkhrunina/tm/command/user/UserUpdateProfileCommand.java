package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "u-update-profile";

    private static final String DESCRIPTION = "Update current user profile.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[Update user profile]");
        System.out.println("Enter first name: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name: ");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter last name: ");
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}