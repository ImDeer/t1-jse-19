package t1.dkhrunina.tm.repository;

import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (projectId.equals(task.getProjectId())) projectTasks.add(task);
        }
        return projectTasks;
    }

}